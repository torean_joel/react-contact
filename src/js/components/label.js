import React from 'react';

const Label = props => <label className={props.labelClass}>{ props.children }</label>

export default Label;