import React from 'react';
import Label from './label';
import Input from './input';
import Button from './button';

//create elements
const inputGenerate = (index,elmClass, labelName) => {
  return (
    <div key={index} className='input-wrapper'>
      <Label>{labelName}</Label> <Input inputName={labelName} className={elmClass}/>
    </div>
  );
};

//loop to create specific ammount of elements
const dynamicFormInputs = props => {
  let elements = [];
  let i = 0, len = props.length;

  for(i; i < len; i++) {
    //add in the whole object here,
    elements.push(inputGenerate(i,props[i].className, props[i].formLabel));
  }

  return elements;
}

const Form = props => {
  return (
    <form onSubmit={props.onSubmit} id={props.formId}>
      {/* Dynamically generate label and input based off parent form component attr */}
      { dynamicFormInputs(props.formInput) }
      <Button buttonClass='submit-button'>Submit</Button>
    </form>
  )
}

export default Form;