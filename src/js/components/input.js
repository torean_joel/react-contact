import React from 'react';

const Input = props => <input name={props.inputName} className={props.className}/>

export default Input;