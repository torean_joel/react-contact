import React from 'react';

const ListItem = props => {
  return (
    <div className="listItem-wrapper">
      <span>{props.name}</span><br/>
      <span>{props.surname}</span><br/>
      <span>{props.address}</span><br/>
      <span>{props.number}</span><br/>
      <br/>
    </div>
  )
}

export default ListItem;