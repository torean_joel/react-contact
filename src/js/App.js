import React, { Component } from 'react';
import '../css/App.css';
//custom component imports
import Title from './components/title';
import Form from './components/form';
import ListItem from './components/listItem';

class App extends Component {
  state = {
    contacts: [{
      name: 'Bob',
      surname: 'green',
      address: 'around the world',
      number: '8495857685'
    },{
      name: 'kyle',
      surname: 'Simpleson',
      address: 'Across the other side',
      number: '7787354382'
    }]
  }

  //form Elements obj to setup form - pass object Through to setup form
  formElementsInputs = [{
    className: 'input-name',
    formLabel: 'name'
  },{
    className: 'input-surname',
    formLabel: 'surname'
  },{
    className: 'input-address',
    formLabel: 'address'
  },{
    className: 'input-number',
    formLabel: 'number'
  }]

  //render contacts in the state
  renderContacts = () => {
    let contacts = this.state.contacts;
    let i = 0, len = contacts.length, listItems = [];

    console.log('contacts', contacts);

    for(i; i < len; i++) {
      listItems.push(<ListItem 
        key={i} 
        name={contacts[i].name}
        surname={contacts[i].surname}
        address={contacts[i].address}
        number={contacts[i].number} />);
    }

    console.log('listItems', listItems);
    return listItems;
  }

  //change the order of the array so the new data is on the top
  reorderContacts = (array) => {
    array.sort((a, b) => {
      return b.id - a.id;
    });

    this.setState({contacts: array});
  }

  //edit fields and create then return JSON structure of the new user
  newContactData = data => {
    let contactsData = [];
    
    //update state with new contacts data
    contactsData.push(...this.state.contacts, data);
    this.reorderContacts(contactsData);
  }

  //submit form values data
  submitForm = (e) => {
    e.preventDefault();

    //get all input fields in form
    let elements = e.target.querySelectorAll('input'), formData = {};

		for( let i = 0; i < elements.length; i++ ) {
      let element = elements[i],
        name = element.name,
        value = element.value;

      if(value) {
        formData[ name ] = value;
      }
    }

    this.newContactData(formData);

    //reset the form
    e.target.reset();
  }

  render() {
    return (
      <div className="App wrapper">
        { /* Lets add a title component below here*/ }
        <Title title='Contacts App'/>
        <hr/>
        { /* Form Comes below here to add users to the contact list */ }
        <Form onSubmit={this.submitForm} formInput={this.formElementsInputs} formId='contacts-form'/>
        <hr/>
        { /* List that will display the data component comes here */ }
        { this.renderContacts() }
      </div>
    );
  }
}

export default App;
