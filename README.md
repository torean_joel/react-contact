##RANDOM REACT CONTACTS - Development
Use the 'npm install' to install all the dependencies for this project then move on to running the build scripts below.

NOTE: The 'npm start' and 'npm run watch' builds need to run simutaniously on separate tabs if you wish you watch for
any style changes you make to sass files that are converted to .css

When making a new component (src/js/components/**.js) that needs separate styling, if you are not going to do the
styling in the component itself be sure to make a new component .sass file in 'src/sass/components/' and import it using
the the relative path in the component to import it i.e "import '../../css/components/title.css';"

## TO DO:

 - Add remove current contact
 - Add edit current contact
 - Add validation
 - Add Styling
 - Clean code and break in smaller junks, remove un needed code


## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `npm run watch`

This script will build the scss (SASS files into the reletive directories as they are in the scss directory but
build them into the css directories that the app will use.

NOTE: All the command will also WATCH any changes to the SCSS files and compile them as you change them

### `npm test`

Launches the test runner in the interactive watch mode.
See the section about [running tests](#running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.
Your app is ready to be deployed!

See the section about [deployment](#deployment) for more information.